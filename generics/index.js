const prompts = require('prompts');
const fse = require('fs-extra');

/*
* A script to allow users to choose the template 
* they want to use and 
*/

async function copyFiles(template, clientAppId) {
    try {
      await fse.copy(
          `${__dirname}/generics/default-template-${template}`, 
          `templates/production/${clientAppId}`)
    } catch (err) {
      console.error(err)
    }
}

function getQuestionsConfig() {
    return [
        {
            type: 'text',
            name: 'clientAppId',
            message: 'What is your client app id?'
        }, {
            type: 'multiselect',
            name: 'template',
            message: 'Which template do you want to use?', 
            choices: [
                { title: 'HIP', value: 't11m' },
                { title: 'Clinical', value: 'clinical' },
                { title: 'Wellness', value: 'wellness' },
            ],
            min: 1,
            max: 1
        }]
}

async function main() {
    const questions = getQuestionsConfig()
    const {template, clientAppId} = await prompts(questions);  
    copyFiles(template, clientAppId)
}


main()
    .catch(e => console.log(e))

